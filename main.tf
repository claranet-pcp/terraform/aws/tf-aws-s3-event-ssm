data "archive_file" "lambda_package" {
  type        = "zip"
  source_file = "${path.module}/include/lambda.py"
  output_path = "${path.cwd}/.terraform/tf-aws-s3-event-ssm-${md5(file("${path.module}/include/lambda.py"))}.zip"
}

resource "aws_lambda_function" "lambda" {
  filename         = "./.terraform/tf-aws-s3-event-ssm-${md5(file("${path.module}/include/lambda.py"))}.zip"
  source_code_hash = "${data.archive_file.lambda_package.output_base64sha256}"
  function_name    = "${random_id.name.hex}"
  role             = "${aws_iam_role.lambda.arn}"
  handler          = "lambda.lambda_handler"
  runtime          = "python2.7"
  timeout          = "${var.lambda_timeout}"

  environment {
    variables = {
      OBJECT_CREATED_COMMANDS            = "${var.object_created_commands}"
      OBJECT_CREATED_TARGET_ENVIRONMENTS = "${var.object_created_target_environments}"
      OBJECT_CREATED_TARGET_SERVICES     = "${var.object_created_target_services}"
      OBJECT_REMOVED_COMMANDS            = "${var.object_removed_commands}"
      OBJECT_REMOVED_TARGET_ENVIRONMENTS = "${var.object_removed_target_environments}"
      OBJECT_REMOVED_TARGET_SERVICES     = "${var.object_removed_target_services}"
    }
  }
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${var.s3_bucket_id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.lambda.arn}"
    events              = ["s3:ObjectCreated:*", "s3:ObjectRemoved:*"]
  }
}
