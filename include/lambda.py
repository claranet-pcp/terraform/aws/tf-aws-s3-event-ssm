import boto3
import json
import logging
import os
from botocore.exceptions import ClientError

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

SSM_PARAMS = {
    'OBJECT_CREATED': {
        'commands': os.environ['OBJECT_CREATED_COMMANDS'],
        'target_environments': os.environ['OBJECT_CREATED_TARGET_ENVIRONMENTS'],
        'target_services': os.environ['OBJECT_CREATED_TARGET_SERVICES'],
    },
    'OBJECT_REMOVED': {
        'commands': os.environ['OBJECT_REMOVED_COMMANDS'],
        'target_environments': os.environ['OBJECT_REMOVED_TARGET_ENVIRONMENTS'],
        'target_services': os.environ['OBJECT_REMOVED_TARGET_SERVICES'],
    }
}

def split_string(string):
    """
    Splits string using a delimiter
    """
    return string.split(',')

def send_run_command(environments, services, commands):
    """
    Sends the Run Command API Call
    """
    try:
        ssm = boto3.client('ssm')
    except ClientError as err:
        LOGGER.error("Run Command Failed!\n%s", str(err))
        return False

    try:
        ssm.send_command(
            Targets=[{
                'Key': 'tag:Environment',
                'Values': environments
            },
            {
                'Key': 'tag:Service',
                'Values': services
            }
            ],
            DocumentName='AWS-RunShellScript',
            TimeoutSeconds=900,
            Parameters={
                'commands': commands,
                'executionTimeout': ['600'] # Seconds all commands have to complete in
            }
        )
        return True
    except ClientError as err:
        if 'ThrottlingException' in str(err):
            LOGGER.info("RunCommand throttled, automatically retrying...")
            send_run_command(environments, services, commands)
        else:
            raise Exception("Run Command Failed!\n%s", str(err))
            return False

def log_event(event):
    """Logs event information for debugging"""
    LOGGER.info("====================================================")
    LOGGER.info(event)
    LOGGER.info("====================================================")

def lambda_handler(event, context):
    """ Lambda Handler """
    log_event(event)

    event_name = event['Records'][0]['eventName']
    key = event['Records'][0]['s3']['object']['key']
    bucket = event['Records'][0]['s3']['bucket']['name']

    if event_name.startswith('ObjectCreated'):
        ssm_params = SSM_PARAMS['OBJECT_CREATED']
    elif event_name.startswith('ObjectRemoved'):
        ssm_params = SSM_PARAMS['OBJECT_REMOVED']
    else:
        raise Exception("Unknown event name: %s", event_name)

    if ssm_params['commands']:
        environments = split_string(ssm_params['target_environments'])
        services = split_string(ssm_params['target_services'])
        commands = split_string(ssm_params['commands'].replace('BUCKET', bucket).replace('KEY', key))
        send_run_command(environments, services, commands)

