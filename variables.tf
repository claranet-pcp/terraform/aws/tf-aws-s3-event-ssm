variable "s3_bucket_arn" {
  description = "The bucket ARN to attach a listener to."
}

variable "s3_bucket_id" {
  description = "The bucket ID to attach a listener to."
}

variable "name" {
  description = "The base name used for created resources."
  type        = "string"
}

variable "random_name_byte_length" {
  description = "The byte length of the random id generator used for unique resource names."
  default     = 4
}

variable "lambda_timeout" {
  description = "Lambda execution timeout in seconds."
  default     = 10
}

variable "object_created_commands" {
  description = "Comma delimited list of commands to send on object creation."
  type        = "string"
  default     = ""
}

variable "object_created_target_environments" {
  description = "Comma delimited list of target environments."
  type        = "string"
  default     = ""
}

variable "object_created_target_services" {
  description = "Comma delimited list of target services."
  type        = "string"
  default     = ""
}

variable "object_removed_commands" {
  description = "Comma delimited list of commands to send on object removal."
  type        = "string"
  default     = ""
}

variable "object_removed_target_environments" {
  description = "Comma delimited list of target environments."
  type        = "string"
  default     = ""
}

variable "object_removed_target_services" {
  description = "Comma delimited list of target services."
  type        = "string"
  default     = ""
}
